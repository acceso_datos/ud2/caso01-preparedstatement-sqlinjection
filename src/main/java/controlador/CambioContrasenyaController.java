package controlador;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import controladorbd.ConexionBD;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class CambioContrasenyaController implements Initializable {
	@FXML
	private Label lbTitulo;
	
	@FXML
	private Button btnCambiar;
	
	@FXML
	private TextField tfUser;
	
	@FXML
	private PasswordField tfPasswd;
	
	Connection con;
	
   	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			con = ConexionBD.getConexion();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			Platform.exit();
		}
	}
   	
   	@FXML
    private void accionEntrar() {
   		int id;
        String usr, pw, sql;
        ResultSet rs;
        Statement st;
        
        try {            
            usr = tfUser.getText();
            pw = tfPasswd.getText();
            
            // Sin PreparedStatement  
            // Peligros 
            // Sentencia: update cliente set password = '1234' where id = 1
            // 	 	 		   				   			 aquí inyectamos   ^ or true;
            // 
            sql = "UPDATE empresa.cliente set password = '" + pw + "' where id = " + usr;
            System.out.println("Sentencia final: " + sql);
            st = con.createStatement();
            int filasAfectadas = st.executeUpdate(sql);
            if (filasAfectadas > 0) {
            	System.out.println("Contraseña cambiada satisfactoriamente");
            }

            // ----------------------------------------------
            // Con PreparedStatement
//            PreparedStatement pst;
//            id = Integer.parseInt(tfUser.getText());
//            pw = tfPasswd.getText();
//            sql = "UPDATE empresa.cliente set password = ? where id = ?";
//            pst = con.prepareStatement(sql);
//            pst.setString(1, pw);
//            pst.setInt(2, id);
//            System.out.println(pst.toString());
//            rs = pst.executeQuery();
//            int filasAfectadas = st.executeUpdate(sql);
//            if (filasAfectadas > 0) {
//            	System.out.println("Contraseña cambiada satisfactoriamente");
//            }          

            
        } catch (SQLException ex) {
            System.out.println("Error SQL: " + ex.getMessage());
        }
   	}
}
